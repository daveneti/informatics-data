package uno.informatics.data.io;

import java.beans.PropertyChangeSupport;
import java.io.File;

import uno.informatics.data.io.FileType;

public class FileProperties
{
	public static final String FILE_PROPERTY = FileProperties.class.getName() + ".file";
	public static final String FILE_TYPE_PROPERTY = FileProperties.class.getName() + ".fileType";

	private File file ;
	private PropertyChangeSupport propertyChangeSupport ;

	private FileType fileType = FileType.TXT;
	
	public FileProperties(File file)
  {
		this(file, FileType.TXT) ;
  }
	
	public FileProperties(File file, FileType fileType)
  {
	  super();
	  propertyChangeSupport = new PropertyChangeSupport(this) ;
	  this.file = file;
	  this.fileType = fileType ;
  }

	public final File getFile()
  {
  	return file;
  }

	public final void setFile(File file)
  {
		if (this.file != file)
		{
			File oldValue = this.file ;
			this.file = file;
	  	
			propertyChangeSupport.firePropertyChange(FILE_PROPERTY, oldValue, this.file = file) ;
		}
  }
	
	public final FileType getFileType()
  {
  	return fileType ;
  }

	public final void setFileType(FileType fileType)
  {
		if (this.fileType != fileType)
		{
			validateFileType(fileType) ;
			FileType oldValue = this.fileType ;
			this.fileType = fileType;
			
			propertyChangeSupport.firePropertyChange(FILE_TYPE_PROPERTY, oldValue, this.fileType) ;
		}
  }
	
	@Override
  public int hashCode()
  {
	  final int prime = 31;
	  int result = 1;
	  result = prime * result + ((file == null) ? 0 : file.hashCode());
	  result = prime * result + ((fileType == null) ? 0 : fileType.hashCode());
	  return result;
  }

	@Override
  public boolean equals(Object obj)
  {
	  if (this == obj)
		  return true;
	  if (obj == null)
		  return false;
	  if (getClass() != obj.getClass())
		  return false;
	  FileProperties other = (FileProperties) obj;
	  if (file == null)
	  {
		  if (other.file != null)
			  return false;
	  }
	  else
		  if (!file.equals(other.file))
			  return false;
	  if (fileType != other.fileType)
		  return false;
	  return true;
  }

	protected void validateFileType(FileType fileType) throws IllegalArgumentException
  {

  }

	protected final PropertyChangeSupport getPropertyChangeSupport()
  {
  	return propertyChangeSupport;
  }
	
}
