package uno.informatics.data.dataset;

public enum ScaleType
{
	NONE ("None"),
	NOMINAL ("Nominal"),
	ORDINAL ("Ordinal"),
	INTERVAL ("Interval"),
	RATIO ("Ratio") ;
	
	private String name ;

	private ScaleType (String name)
	{
		this.name = name  ;
	}

	public String getName()
	{
		return name;
	}
}
