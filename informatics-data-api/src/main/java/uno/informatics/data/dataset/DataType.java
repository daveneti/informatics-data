package uno.informatics.data.dataset;

public enum DataType
{
	BOOLEAN ("Boolean"),
	SHORT ("Short"),
	INTEGER ("Integer"),
	LONG ("Long"),
	FLOAT ("Float"),
	DOUBLE ("Integer"),
	BIG_INTEGER ("Big Integer"),
	BIG_DECIMAL ("Big Decimal"),
	DATE ("Date"),
	LSID ("LSID"),
	STRING ("String") ;
	
	private String name ;

	private DataType (String name)
	{
		this.name = name  ;
	}

	public String getName()
	{
		return name;
	}
}
