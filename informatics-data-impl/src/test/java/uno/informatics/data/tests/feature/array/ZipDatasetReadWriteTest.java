/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.tests.feature.array;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import uno.informatics.data.dataset.DataType;
import uno.informatics.data.dataset.FeatureDataset;
import uno.informatics.data.dataset.DatasetException;
import uno.informatics.data.dataset.FeatureDatasetRow;
import uno.informatics.data.dataset.DatasetFeature;
import uno.informatics.data.feature.array.ArrayFeatureDataset;
import uno.informatics.data.feature.array.ZipFeatureDatasetReader;
import uno.informatics.data.feature.array.ZipFeatureDatasetWriter;
import uno.informatics.data.io.DatasetReader;
import uno.informatics.data.io.DatasetWriter;
import uno.informatics.data.io.FileType;
import uno.informatics.data.tests.feature.TestData;

/**
 * @author Guy Davenport
 *
 */
public class ZipDatasetReadWriteTest extends TestData
{
	private static final String FILE = "target/test.zip" ;
	
	@Test
	public void testWriteReadTxt()
	{
		testWriteRead(FileType.TXT) ;
	}
	
	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.ZipFeatureDatasetReader#read()} and 
	 * {@link uno.informatics.data.tests.feature.array.ZipFeatureDatasetWriter#write(uno.informatics.data.feature.FeatureDataset)}..
	 */
	public void testWriteRead(FileType fileType)
	{
		FeatureDataset originalDataset = createDataset() ;
		
		ZipFeatureDatasetWriter writer = new ZipFeatureDatasetWriter(new File(FILE)) ;
		
		writer.setFileType(fileType) ;

		try
    {
			writer.write(originalDataset) ;
			
			ZipFeatureDatasetReader reader = new ZipFeatureDatasetReader(new File(FILE)) ;
				
	    FeatureDataset readDataset = (FeatureDataset)reader.read() ;
	    
			assertEquals(originalDataset.getUniqueIdentifier(), readDataset.getUniqueIdentifier()) ;
			assertEquals(originalDataset.getName(), readDataset.getName()) ;
			assertEquals(originalDataset.getDescription(), readDataset.getDescription()) ;
			
			FeatureDatasetRow[] originalRows = originalDataset.getRowsAsArray() ;
			FeatureDatasetRow[] readRows = readDataset.getRowsAsArray() ;
			
			assertEquals(originalDataset.getRowCount(), readDataset.getRowCount()) ;
			
			for (int i = 0 ; i < originalRows.length ; ++i)
				assertArrayEquals(originalRows[i].getValuesAsArray(), readRows[i].getValuesAsArray()) ;
			
			List<DatasetFeature> originalFeatures = originalDataset.getFeatures() ;
			List<DatasetFeature> readFeatures = readDataset.getFeatures();
			
			assertEquals(originalFeatures.size(), readFeatures.size()) ;
			
			Iterator<DatasetFeature> iterator1 = originalFeatures.iterator() ;
			Iterator<DatasetFeature> iterator2 = readFeatures.iterator() ;
			
			while (iterator1.hasNext() && iterator2.hasNext())
			{
				assertFeatureEquals(iterator1.next(), iterator2.next()) ;
			}			
    }
    catch (DatasetException e)
    {
    	e.printStackTrace();
    	fail(e.getMessage()) ;
    }
	}
	
  /**
	 * @param next
	 * @param next2
	 */
  private void assertFeatureEquals(DatasetFeature feaure1, DatasetFeature feature2)
  {
		assertEquals(feaure1.getUniqueIdentifier(), feature2.getUniqueIdentifier()) ;
		assertEquals(feaure1.getName(), feature2.getName()) ;
		assertEquals(feaure1.getDescription(), feature2.getDescription()) ;
		assertEquals(feaure1.getDataType(), feature2.getDataType()) ;
		assertEquals(feaure1.getScaleType(), feature2.getScaleType()) ;
  }

	protected FeatureDataset createDataset()
  {
	  return new ArrayFeatureDataset(UID, NAME, DESCRIPTION, FEATURES, OBJECT_TABLE_AS_ARRAY);
  }
}
