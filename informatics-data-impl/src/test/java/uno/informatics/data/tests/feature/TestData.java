/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.tests.feature;

import java.util.ArrayList;
import java.util.List;

import uno.informatics.data.dataset.DataType;
import uno.informatics.data.dataset.FeatureDatasetRow;
import uno.informatics.data.dataset.DatasetFeature;
import uno.informatics.data.dataset.ScaleType;
import uno.informatics.data.feature.DatasetFeatureImpl;
import uno.informatics.data.feature.array.ArrayDatasetRow;

/**
 * @author Guy Davenport
 *
 */
public class TestData
{
	protected final static String UID = "uid" ;
	protected final static String NAME = "name" ;
	protected final static String DESCRIPTION = "description" ;
	
	protected final static String ROW1_NAME = "row1" ;
	protected final static String ROW2_NAME = "row2" ;
	protected final static String ROW3_NAME = "row3" ;
	
	protected final static Object[] OBJECT_ROW1 = new Object[] {1,1.1,"R1C3", true, "12/12/2012"} ;
	protected final static Object[] OBJECT_ROW2 = new Object[] {2,2.2,"R2C3", false, "13/12/2012"} ;
	protected final static Object[] OBJECT_ROW3 = new Object[] {3,3.3,"R3C3", true, "14/12/2012"} ;

	protected final static Object[][] OBJECT_TABLE_AS_ARRAY = new Object[][] {OBJECT_ROW1, OBJECT_ROW2, OBJECT_ROW3} ;
	
	protected static final List<List<Object>> OBJECT_TABLE_AS_LIST = new ArrayList<List<Object>>();
	
	static
	{
		OBJECT_TABLE_AS_LIST.add(new ArrayList<Object>(OBJECT_ROW1.length)) ;
		
		OBJECT_TABLE_AS_LIST.get(0).add(OBJECT_ROW1[0]) ;
		OBJECT_TABLE_AS_LIST.get(0).add(OBJECT_ROW1[1]) ;
		OBJECT_TABLE_AS_LIST.get(0).add(OBJECT_ROW1[2]) ;
		OBJECT_TABLE_AS_LIST.get(0).add(OBJECT_ROW1[3]) ;
		OBJECT_TABLE_AS_LIST.get(0).add(OBJECT_ROW1[4]) ;
		
		OBJECT_TABLE_AS_LIST.add(new ArrayList<Object>(OBJECT_ROW2.length)) ;
		
		OBJECT_TABLE_AS_LIST.get(1).add(OBJECT_ROW2[0]) ;
		OBJECT_TABLE_AS_LIST.get(1).add(OBJECT_ROW2[1]) ;
		OBJECT_TABLE_AS_LIST.get(1).add(OBJECT_ROW2[2]) ;
		OBJECT_TABLE_AS_LIST.get(1).add(OBJECT_ROW2[3]) ;
		OBJECT_TABLE_AS_LIST.get(1).add(OBJECT_ROW2[4]) ;
		
		OBJECT_TABLE_AS_LIST.add(new ArrayList<Object>(OBJECT_ROW2.length)) ;
		
		OBJECT_TABLE_AS_LIST.get(2).add(OBJECT_ROW3[0]) ;
		OBJECT_TABLE_AS_LIST.get(2).add(OBJECT_ROW3[1]) ;
		OBJECT_TABLE_AS_LIST.get(2).add(OBJECT_ROW3[2]) ;
		OBJECT_TABLE_AS_LIST.get(2).add(OBJECT_ROW3[3]) ;
		OBJECT_TABLE_AS_LIST.get(2).add(OBJECT_ROW3[4]) ;
	}
	
	protected final static FeatureDatasetRow ROW_WITH_NAME1 = new ArrayDatasetRow(ROW1_NAME, OBJECT_ROW1)  ;
	protected final static FeatureDatasetRow ROW_WITH_NAME2 = new ArrayDatasetRow(ROW2_NAME, OBJECT_ROW2)  ;
	protected final static FeatureDatasetRow ROW_WITH_NAME3 = new ArrayDatasetRow(ROW3_NAME, OBJECT_ROW3)  ;
	
	protected final static FeatureDatasetRow ROW1 = new ArrayDatasetRow(OBJECT_ROW1)  ;
	protected final static FeatureDatasetRow ROW2 = new ArrayDatasetRow(OBJECT_ROW2)  ;
	protected final static FeatureDatasetRow ROW3 = new ArrayDatasetRow(OBJECT_ROW3)  ;

	protected final static FeatureDatasetRow[] ROWS_AS_ARRAY = new FeatureDatasetRow[]{ROW1, ROW2, ROW3} ;
	
	protected static final List<FeatureDatasetRow> ROWS_AS_LIST = new ArrayList<FeatureDatasetRow>();

	static
	{
		ROWS_AS_LIST.add(ROW1) ;
		ROWS_AS_LIST.add(ROW2) ;
		ROWS_AS_LIST.add(ROW3) ;
	}
	
	protected static final List<DatasetFeature> FEATURES = new ArrayList<DatasetFeature>();
	
	static
	{
		FEATURES.add(new DatasetFeatureImpl("Col1", DataType.INTEGER, ScaleType.INTERVAL)) ;
		FEATURES.add(new DatasetFeatureImpl("Col2", DataType.DOUBLE, ScaleType.RATIO)) ;
		FEATURES.add(new DatasetFeatureImpl("Col3", DataType.STRING, ScaleType.NOMINAL)) ;
		FEATURES.add(new DatasetFeatureImpl("Col4", DataType.BOOLEAN, ScaleType.NOMINAL)) ;
		FEATURES.add(new DatasetFeatureImpl("Col5", DataType.DATE, ScaleType.NOMINAL)) ;
	}
}
