/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.tests.feature.array;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import uno.informatics.data.dataset.FeatureDataset;
import uno.informatics.data.dataset.FeatureDatasetRow;
import uno.informatics.data.feature.array.ArrayFeatureDataset;

/**
 * @author Guy Davenport
 *
 */
public class ArrayDatasetTest extends DatasetTest
{
	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.ArrayFeatureDataset.ArrayDataset#ArrayDataset(java.lang.String, java.util.List, java.lang.Object[][])}.
	 */
	@Test
	public void testArrayDatasetStringListOfFeatureObjectArrayArray()
	{
		ArrayFeatureDataset dataset = new ArrayFeatureDataset(NAME, FEATURES, OBJECT_TABLE_AS_ARRAY) ;
				
		assertEquals(NAME, dataset.getName()) ;
		assertEquals(null, dataset.getDescription()) ;
		
		FeatureDatasetRow[] rows = dataset.getRowsAsArray() ;
		
		assertEquals(OBJECT_TABLE_AS_ARRAY.length, dataset.getRowCount()) ;
		
		for (int i = 0 ; i < rows.length ; ++i)
			assertArrayEquals(OBJECT_TABLE_AS_ARRAY[i], rows[i].getValuesAsArray()) ;
	}

	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.ArrayFeatureDataset.ArrayDataset#ArrayDataset(java.lang.String, java.lang.String, java.util.List, java.lang.Object[][])}.
	 */
	@Test
	public void testArrayDatasetStringStringListOfFeatureObjectArrayArray()
	{
		ArrayFeatureDataset dataset = new ArrayFeatureDataset(UID, NAME, FEATURES, OBJECT_TABLE_AS_ARRAY) ;
		
		assertEquals(UID, dataset.getUniqueIdentifier()) ;
		assertEquals(NAME, dataset.getName()) ;
		assertEquals(null, dataset.getDescription()) ;
		
		FeatureDatasetRow[] rows = dataset.getRowsAsArray() ;
		
		assertEquals(OBJECT_TABLE_AS_ARRAY.length, dataset.getRowCount()) ;
		
		for (int i = 0 ; i < rows.length ; ++i)
			assertArrayEquals(OBJECT_TABLE_AS_ARRAY[i], rows[i].getValuesAsArray()) ;
	}
	
	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.ArrayFeatureDataset.ArrayDataset#ArrayDataset(java.lang.String, java.lang.String, java.lang.String, java.util.List, java.lang.Object[][])}.
	 */
	@Test
	public void testArrayDatasetStringStringStringListOfFeatureObjectArrayArray()
	{
		ArrayFeatureDataset dataset = new ArrayFeatureDataset(UID, NAME, DESCRIPTION, FEATURES, OBJECT_TABLE_AS_ARRAY) ;
		
		assertEquals(UID, dataset.getUniqueIdentifier()) ;
		assertEquals(NAME, dataset.getName()) ;
		assertEquals(DESCRIPTION, dataset.getDescription()) ;
		
		FeatureDatasetRow[] rows = dataset.getRowsAsArray() ;
		
		assertEquals(OBJECT_TABLE_AS_ARRAY.length, dataset.getRowCount()) ;
		
		for (int i = 0 ; i < rows.length ; ++i)
			assertArrayEquals(OBJECT_TABLE_AS_ARRAY[i], rows[i].getValuesAsArray()) ;
	}
	
	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.array.ArrayDatasetRow#setName(java.lang.String)}.
	 */
	@Test
	public void testSetName()
	{
		ArrayFeatureDataset dataset = new ArrayFeatureDataset(UID, NAME, DESCRIPTION, FEATURES, OBJECT_TABLE_AS_ARRAY) ;
		
		assertEquals(NAME, dataset.getName()) ;
		
		dataset.setName("asdf") ;
		
		assertEquals("asdf", dataset.getName()) ;
	}
	
	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.DatasetTest#createDataset()
	 */
  @Override
  protected FeatureDataset createDataset()
  {
	  return new ArrayFeatureDataset(NAME, FEATURES, OBJECT_TABLE_AS_ARRAY);
  }
}
