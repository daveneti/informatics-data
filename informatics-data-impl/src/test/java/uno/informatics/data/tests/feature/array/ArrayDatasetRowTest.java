/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.tests.feature.array;

import static org.junit.Assert.*;

import org.junit.Test;

import uno.informatics.data.dataset.FeatureDatasetRow;
import uno.informatics.data.feature.array.ArrayDatasetRow;

/**
 * @author Guy Davenport
 *
 */
public class ArrayDatasetRowTest extends DatasetRowTest
{
	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.array.ArrayDatasetRow#ArrayDatasetRow(java.lang.Object[])}.
	 */
	@Test
	public void testArrayDatasetRowObjectArray()
	{
		FeatureDatasetRow datasetRow = new ArrayDatasetRow(OBJECT_ROW1) ;
		
		assertArrayEquals(OBJECT_ROW1, datasetRow.getValuesAsArray()) ;
		assertEquals(null, datasetRow.getName()) ;
	}

	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.array.ArrayDatasetRow#ArrayDatasetRow(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testArrayDatasetRowStringObjectArray()
	{
		FeatureDatasetRow datasetRow = new ArrayDatasetRow(OBJECT_ROW1) ;
		
		assertArrayEquals(OBJECT_ROW1, datasetRow.getValuesAsArray()) ;
		assertEquals(null, datasetRow.getName()) ;
	}

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.DatasetRowTest#createDatasetRow()
	 */
  @Override
  protected FeatureDatasetRow createDatasetRow()
  {
	  return new ArrayDatasetRow(ROW1_NAME, OBJECT_ROW1);
  }

}
