/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.tests.feature;

import static org.junit.Assert.*;

import org.junit.Test;

import uno.informatics.data.dataset.DataType;
import uno.informatics.data.dataset.DatasetFeature;
import uno.informatics.data.dataset.ScaleType;
import uno.informatics.data.feature.DatasetFeatureImpl;

/**
 * @author Guy Davenport
 *
 */
public class FeatureImplTest
{
	protected final static String UID = "uid" ;
	protected final static String NAME = "name" ;
	protected final static String DESCRIPTION = "description" ;
	
	/**
	 * Test method for {@link uno.informatics.model.impl.IdentifierImpl#getUniqueIdentifier()}.
	 */
	@Test
	public void testGetUniqueIdentifier()
	{
		DatasetFeature feature = new DatasetFeatureImpl(UID, NAME, DataType.BOOLEAN, ScaleType.INTERVAL) ;
		
		assertEquals(UID, feature.getUniqueIdentifier()) ;
	}

	/**
	 * Test method for {@link uno.informatics.model.impl.IdentifierImpl#getName()}.
	 */
	@Test
	public void testGetName()
	{
		DatasetFeature feature = new DatasetFeatureImpl(NAME, DataType.BOOLEAN, ScaleType.INTERVAL) ;
		
		assertEquals(NAME, feature.getName()) ;
	}

	/**
	 * Test method for {@link uno.informatics.model.impl.IdentifierImpl#getDescription()}.
	 */
	@Test
	public void testGetDescription()
	{
		DatasetFeature feature = new DatasetFeatureImpl(UID, NAME, DESCRIPTION, DataType.BOOLEAN, ScaleType.INTERVAL) ;
		
		assertEquals(DESCRIPTION, feature.getDescription()) ;
	}

	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.DatasetFeatureImpl.FeatureImpl#getDataType()}.
	 */
	@Test
	public void testGetDataType()
	{
		DatasetFeature feature = new DatasetFeatureImpl("test", DataType.BOOLEAN, ScaleType.INTERVAL) ;
		
		assertEquals(DataType.BOOLEAN, feature.getDataType()) ;
	}

	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.DatasetFeatureImpl.FeatureImpl#setDataType(uno.informatics.data.tests.feature.array.array.DataType)}.
	 */
	@Test
	public void testSetDataType()
	{
		DatasetFeatureImpl feature = new DatasetFeatureImpl("test", DataType.BOOLEAN, ScaleType.INTERVAL) ;
		
		assertEquals(DataType.BOOLEAN, feature.getDataType()) ;
		
		feature.setDataType(DataType.BIG_DECIMAL) ;
		
		assertEquals(DataType.BIG_DECIMAL, feature.getDataType()) ;
	}

	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.DatasetFeatureImpl.FeatureImpl#getScaleType()}.
	 */
	@Test
	public void testGetScaleType()
	{
		DatasetFeatureImpl feature = new DatasetFeatureImpl("test", DataType.BOOLEAN, ScaleType.INTERVAL) ;
		
		assertEquals(ScaleType.INTERVAL, feature.getScaleType()) ;
	}

	/**
	 * Test method for {@link uno.informatics.data.tests.feature.array.DatasetFeatureImpl.FeatureImpl#setScaleType(uno.informatics.data.tests.feature.array.array.ScaleType)}.
	 */
	@Test
	public void testSetScaleType()
	{
		DatasetFeatureImpl feature = new DatasetFeatureImpl("test", DataType.BOOLEAN, ScaleType.INTERVAL) ;
		
		assertEquals(ScaleType.INTERVAL, feature.getScaleType()) ;
		
		feature.setScaleType(ScaleType.NOMINAL) ;
		
		assertEquals(ScaleType.NOMINAL, feature.getScaleType()) ;
	}

}
