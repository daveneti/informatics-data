/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.feature.array;

import java.util.ArrayList;
import java.util.List;

import uno.informatics.data.dataset.FeatureDatasetRow;

/**
 * @author Guy Davenport
 *
 */
public class ArrayDatasetRow implements FeatureDatasetRow
{
	private String name ;
	private Object[] values ;
	
	public ArrayDatasetRow(Object[] values)
  {
	  this(null, values);
  }
	
	public ArrayDatasetRow(String name, Object[] values)
  {
	  super();
	  
	  setName(name) ;
	  
	  if (values != null)
	  	this.values = values;
	  else
	  	this.values = new Object[0];
  }

	/* (non-Javadoc)
	 * @see uno.informatics.common.Namable#getName()
	 */
	@Override
	public final String getName()
	{
		return name;
	}

	/* (non-Javadoc)
	 * @see uno.informatics.common.Namable#setName(java.lang.String)
	 */
	@Override
	public final void setName(String name)
	{
		this.name = name ;
	}

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.DatasetRow#getValues()
	 */
	@Override
  public final List<Object> getValues()
  {
	  return toList(values);
  }

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.DatasetRow#getValuesAsArray()
	 */
	@Override
  public final Object[] getValuesAsArray()
  {
	  return values;
  }
  
	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.DatasetRow#getValue()
	 */
	@Override
  public final Object getValue(int columnIndex)
  {
	  return values[columnIndex];
  }
	
	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.DatasetRow#getColumnCount()
	 */
	@Override
  public int getColumnCount()
  {
	  // TODO Auto-generated method stub
	  return values.length ;
  }

	/**
	 * @param dataTypes
	 * @return
	 */
  private List<Object> toList(Object[] array)
  {
  	List<Object> list = new ArrayList<Object>(array.length) ;

  	for (int i = 0 ; i < array.length ; ++i)
  		list.add(array[i]) ;
  	
	  return list;
  }
}
