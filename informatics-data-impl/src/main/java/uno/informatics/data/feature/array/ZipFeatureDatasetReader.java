/*******************************************************************************
 * Copyright 2010 Guy Davenport Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *******************************************************************************/
package uno.informatics.data.feature.array;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import com.thoughtworks.xstream.XStream;

import uno.informatics.common.io.RowReader;
import uno.informatics.common.io.TableReader;
import uno.informatics.common.io.TextFileHandler;
import uno.informatics.common.io.jxl.JXLExcelRowStringReader;
import uno.informatics.common.io.jxl.JXLExcelTableObjectReader;
import uno.informatics.common.io.text.TextFileRowStringReader;
import uno.informatics.common.io.text.TextFileTableObjectReader;
import uno.informatics.data.dataset.FeatureDataset;
import uno.informatics.data.dataset.DatasetException;
import uno.informatics.data.dataset.DatasetFeature;
import uno.informatics.data.io.DatasetReader;
import uno.informatics.data.io.ExcelFileProperties;
import uno.informatics.data.io.FileType;
import uno.informatics.model.Dataset;
import uno.informatics.model.Identifier;
import uno.informatics.model.impl.IdentifierImpl;

/**
 * @author Guy Davenport
 */
public class ZipFeatureDatasetReader extends ZipFileHandler implements DatasetReader
{
	private static final String	SPREADSHEET_NAME	= "values";

	/**
	 * @param file
	 */
	public ZipFeatureDatasetReader(File file)
	{
		super(file);
	}

	/*
	 * (non-Javadoc)
	 * @see uno.informatics.data.io.DatasetWriter#read()
	 */
	@Override
	public Dataset read() throws DatasetException
	{
		FeatureDataset dataset = null;

		try
		{
			ZipFile zipFile = new ZipFile(getFile());

			XStream xstream = createXStream();

			ZipEntry zipEntry = zipFile.getEntry(IDENTIFICATION_ENTRY);

			Identifier identification = (Identifier) xstream.fromXML(zipFile
			    .getInputStream(zipEntry));

			zipEntry = zipFile.getEntry(FEATURES_ENTRY);

			@SuppressWarnings("unchecked")
			List<DatasetFeature> features = (List<DatasetFeature>) xstream.fromXML(zipFile
			    .getInputStream(zipEntry));

			zipEntry = zipFile.getEntry(FILE_TYPE_ENTRY);

			FileType fileType = (FileType) xstream.fromXML(zipFile
			    .getInputStream(zipEntry));

			TableReader<Object> reader = null;

			switch (fileType)
			{
				case CSV:
					zipEntry = zipFile.getEntry(DATA_VALUES_ENTRY_PREIFX + CSV_SUFFIX);

					TextFileTableObjectReader textFileRowStringReader = new TextFileTableObjectReader(
					    new BufferedReader(new InputStreamReader(
					        zipFile.getInputStream(zipEntry))));

					textFileRowStringReader.setDelimiterString(TextFileHandler.COMMA);

					reader = textFileRowStringReader;
					break;
				case TXT:
					zipEntry = zipFile.getEntry(DATA_VALUES_ENTRY_PREIFX + TXT_SUFFIX);

					textFileRowStringReader = new TextFileTableObjectReader(
					    new BufferedReader(new InputStreamReader(
					        zipFile.getInputStream(zipEntry))));

					textFileRowStringReader.setDelimiterString(TextFileHandler.TAB);

					reader = textFileRowStringReader;
					break;
				case XLS:
					/*
					 * zipEntry = zipFile.getEntry(DATA_VALUES_ENTRY_PREIFX + XLS_SUFFIX)
					 * ; JXLExcelTableObjectReader jxlExcelTableObjectReader = new
					 * JXLExcelTableObjectReader(new BufferedReader(new
					 * InputStreamReader(zipFile.getInputStream(zipEntry)))) ;
					 * jxlExcelTableObjectReader.setSpreadSheetName(SPREADSHEET_NAME);
					 * reader = jxlExcelTableObjectReader ;
					 */
					break;
				case XLSX:
					/*
					 * zipEntry = zipFile.getEntry(DATA_VALUES_ENTRY_PREIFX + XLSX_SUFFIX)
					 * ; jxlExcelTableObjectReader = new JXLExcelTableObjectReader(new
					 * BufferedReader(new
					 * InputStreamReader(zipFile.getInputStream(zipEntry)))) ;
					 * jxlExcelTableObjectReader.setSpreadSheetName(SPREADSHEET_NAME);
					 * reader = jxlExcelTableObjectReader ;
					 */
					break;
				default:
					break;
			}

			Object[][] values = reader.readCellsAsArray();
			
			reader.close(); 

			zipFile.close();

			dataset = createDataset(identification, features, values);

		}
		catch (ZipException e)
		{
			throw new DatasetException(e);
		}
		catch (IOException e)
		{
			throw new DatasetException(e);
		}

		return dataset;
	}

	/**
	 * @param identification
	 * @param features
	 * @param values
	 * @return
	 */
	private FeatureDataset createDataset(Identifier identification,
	    List<DatasetFeature> features, Object[][] values)
	{
		return new ArrayFeatureDataset(identification, features, values);
	}

}
