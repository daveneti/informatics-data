/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.feature.array;

import java.util.ArrayList;
import java.util.List;

import uno.informatics.data.dataset.FeatureDatasetRow;
import uno.informatics.data.dataset.DatasetFeature;
import uno.informatics.model.Identifier;

/**
 * @author Guy Davenport
 *
 */
public class ArrayFeatureDataset extends AbstractFeatureDataset
{
	private FeatureDatasetRow[] rows;
	private int rowCount;

	public ArrayFeatureDataset(String name, List<DatasetFeature> features, Object[][] values)
  {
	  super(name, features);
	  
	  setValues(values) ;
  }

	public ArrayFeatureDataset(String uniqueIdentifier, String name,
      List<DatasetFeature> features, Object[][] values)
  {
	  super(uniqueIdentifier, name, features);
	  
	  setValues(values) ;
  }
	
	public ArrayFeatureDataset(String uniqueIdentifier, String name, String description,
      List<DatasetFeature> features, Object[][] values)
  {
	  super(uniqueIdentifier, name, description, features);
	  
	  setValues(values) ;
  }
	
	public ArrayFeatureDataset(Identifier identification,
      List<DatasetFeature> features, Object[][] values)
  {
	  super(identification != null ? identification.getUniqueIdentifier() : null, 
	  		identification != null ? identification.getName() : null, 
	  		identification != null ? identification.getDescription() : null, features);
	  
	  setValues(values) ;
  }

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.AbstractDataset#getRowCount()
	 */
	@Override
	public final int getRowCount()
	{
		return rowCount ;
	}
	
	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.AbstractDataset#getRow()
	 */
	@Override
	public FeatureDatasetRow getRow(int rowIndex)
	{
		return rows[rowIndex] ;
	}

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.AbstractDataset#getRowsAsArray()
	 */
	@Override
	public final FeatureDatasetRow[] getRowsAsArray()
	{
		return rows ;
	}

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.AbstractDataset#getRows()
	 */
	@Override
	public final List<FeatureDatasetRow> getRows()
	{
		return toRowList(rows);
	}
	
	/* (non-Javadoc)
	 * @see uno.informatics.data.feature.FeatureDataset#getValues()
	 */
  @Override
  public List<List<Object>> getValues()
  {
	  return toObjectListList(rows) ;
  }

	/* (non-Javadoc)
	 * @see uno.informatics.data.feature.FeatureDataset#getValuesAsArray()
	 */
  @Override
  public Object[][] getValuesAsArray()
  {
	  return toObjectArray(rows) ;
  }

	/**
	 * @param values
	 */
  private final void setValues(Object[][] values)
  {
  	rowCount = 0 ;
  	
  	if (values != null)
  	{
  		rowCount = values.length ;
  		
  		rows = new FeatureDatasetRow[rowCount] ;
  		
  		for (int i = 0 ; i < values.length ; ++i)
  			rows[i] = new ArrayDatasetRow(values[i]) ;
  	}
  }

  private List<FeatureDatasetRow> toRowList(FeatureDatasetRow[] array)
  {
  	List<FeatureDatasetRow> list = new ArrayList<FeatureDatasetRow>(array.length) ;

  	for (int i = 0 ; i < array.length ; ++i)
  		list.add(array[i]) ;
  	
	  return list;
  }
  
  private List<List<Object>> toObjectListList(FeatureDatasetRow[] array)
  {
  	List<List<Object>> list = new ArrayList<List<Object>>(array.length) ;

  	for (int i = 0 ; i < array.length ; ++i)
  		list.add(toObjectList(array[i].getValuesAsArray())) ;
  	
	  return list;
  }
  
  private List<Object> toObjectList(Object[] array)
  {
  	List<Object> list = new ArrayList<Object>(array.length) ;

  	for (int i = 0 ; i < array.length ; ++i)
  		list.add(array[i]) ;
  	
	  return list;
  }
  
  private Object[][] toObjectArray(FeatureDatasetRow[] array)
  {
  	Object[][] list = new Object[array.length][] ;

  	for (int i = 0 ; i < array.length ; ++i)
  		list[i] = array[i].getValuesAsArray() ;
  	
	  return list;
  }
}
