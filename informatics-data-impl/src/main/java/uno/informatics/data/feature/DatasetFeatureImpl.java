/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.feature;

import uno.informatics.data.dataset.DataType;
import uno.informatics.data.dataset.DatasetFeature;
import uno.informatics.data.dataset.ScaleType;
import uno.informatics.model.impl.FeatureImpl;

public class DatasetFeatureImpl extends FeatureImpl implements DatasetFeature
{
	private DataType dataType ;
	private ScaleType scaleType ;

	public DatasetFeatureImpl(String name, DataType dataType, ScaleType scaleType)
  {
	  super(name);
	  
	  setDataType(dataType) ;
	  setScaleType(scaleType) ;
  }
	
	public DatasetFeatureImpl(String uniqueIdentifier, String name, DataType dataType, ScaleType scaleType)
  {
	  super(uniqueIdentifier, name);

	  setDataType(dataType) ;
	  setScaleType(scaleType) ;
  }
	
	public DatasetFeatureImpl(String uniqueIdentifier, String name, String description, DataType dataType, ScaleType scaleType)
  {
	  super(uniqueIdentifier, name, description);
	  
	  setDataType(dataType) ;
	  setScaleType(scaleType) ;
  }
	
	public DatasetFeatureImpl(DatasetFeature feature)
  {
	  super(feature);
	  
	  setDataType(feature.getDataType()) ;
	  setScaleType(feature.getScaleType()) ;
  }

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.Feature#getDataType()
	 */
	@Override
  public final DataType getDataType()
  {
	  return dataType;
  }

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.Feature#setDataType(uno.informatics.data.tests.feature.array.array.DataType)
	 */
  public final void setDataType(DataType dataType)
  {
	  this.dataType = dataType ;
  }

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.Feature#getScaleType()
	 */
	@Override
  public final ScaleType getScaleType()
  {
	  return scaleType;
  }

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.Feature#setScaleType(uno.informatics.data.tests.feature.array.array.ScaleType)
	 */
  public final void setScaleType(ScaleType scaleType)
  {
	  this.scaleType = scaleType ;
  }

}
