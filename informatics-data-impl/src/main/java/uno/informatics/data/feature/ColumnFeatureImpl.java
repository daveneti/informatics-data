package uno.informatics.data.feature;
/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/


import uno.informatics.data.dataset.DataType;
import uno.informatics.data.dataset.ScaleType;

/**
 * @author Guy Davenport
 *
 */
public class ColumnFeatureImpl extends DatasetFeatureImpl implements ColumnFeature
{
	private int possibleDataTypes ;
	
	public ColumnFeatureImpl(String name, int possibleDataTypes)
  {
	  super(name, DataType.STRING, ScaleType.NOMINAL);
  }

	public ColumnFeatureImpl(String uniqueIdentifier, String name,
      String description, DataType dataType, ScaleType scaleType)
  {
	  super(uniqueIdentifier, name, description, dataType, scaleType);
  }

	/* (non-Javadoc)
	 * @see uno.informatics.ui.data.ColumnFeature#getPossibleDataTypes()
	 */
	@Override
	public int getPossibleDataTypes()
	{
		return possibleDataTypes ;
	}

	
	/* (non-Javadoc)
	 * @see uno.informatics.ui.data.ColumnFeature#setPossibleDataTypes(int)
	 */
	@Override
	public void setPossibleDataTypes(int possibleDataTypes)
	{
		this.possibleDataTypes = possibleDataTypes ;
	}

}
