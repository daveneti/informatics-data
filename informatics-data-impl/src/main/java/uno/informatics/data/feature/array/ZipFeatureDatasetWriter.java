/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.feature.array;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import uno.informatics.common.io.TableReader;
import uno.informatics.common.io.TableWriter;
import uno.informatics.common.io.TextFileHandler;
import uno.informatics.common.io.jxl.JXLExcelTableObjectReader;
import uno.informatics.common.io.text.TextFileTableObjectReader;
import uno.informatics.common.io.text.TextFileTableObjectWriter;
import uno.informatics.data.dataset.FeatureDataset;
import uno.informatics.data.dataset.DatasetException;
import uno.informatics.data.io.DatasetReader;
import uno.informatics.data.io.DatasetWriter;
import uno.informatics.data.io.FileType;
import uno.informatics.model.Dataset;
import uno.informatics.model.impl.IdentifierImpl;
import uno.informatics.data.dataset.FeatureDataset;

/**
 * @author Guy Davenport
 *
 */
public class ZipFeatureDatasetWriter extends ZipFileHandler implements DatasetWriter
{
	private FileType fileType ;

	/**
	 * @param file
	 */
	public ZipFeatureDatasetWriter(File file)
	{
		super(file);

		setFileType(FileType.TXT) ;
	}

	public final FileType getFileType()
	{
		return fileType;
	}

	public final void setFileType(FileType fileType)
	{
		this.fileType = fileType;
	}

	/* (non-Javadoc)
	 * @see uno.informatics.data.io.DatasetWriter#write(uno.informatics.data.tests.feature.array.Dataset)
	 */
	@Override
	public void write(Dataset dataset) throws DatasetException
	{
		if (dataset instanceof FeatureDataset)
		{
			FeatureDataset featureDataset = (FeatureDataset)dataset ;
			
			try
			{
				ZipOutputStream outputStream = new ZipOutputStream(new FileOutputStream(getFile())) ;
	
				XStream xstream = createXStream() ;
	
				outputStream.putNextEntry(new ZipEntry(FEATURES_ENTRY)) ;
	
				xstream.toXML(featureDataset.getFeatures(), outputStream) ;
	
				outputStream.closeEntry();
	
				outputStream.putNextEntry(new ZipEntry(IDENTIFICATION_ENTRY)) ;
	
				xstream.toXML(new IdentifierImpl(dataset), outputStream) ;
	
				outputStream.closeEntry();
	
				outputStream.putNextEntry(new ZipEntry(FILE_TYPE_ENTRY)) ;
	
				xstream.toXML(fileType, outputStream) ;
	
				outputStream.closeEntry();
	
				TableWriter<Object> writer = null ;
	
				switch (fileType)
				{
					case CSV:
						outputStream.putNextEntry(new ZipEntry(DATA_VALUES_ENTRY_PREIFX + CSV_SUFFIX)) ;
	
						TextFileTableObjectWriter textFileTableObjectWriter = 
								new TextFileTableObjectWriter(new BufferedWriter(new OutputStreamWriter(outputStream))) ;
	
						textFileTableObjectWriter.setDelimiterString(TextFileHandler.COMMA) ;
	
						writer = textFileTableObjectWriter ;
						break;
					case TXT:
	
						outputStream.putNextEntry(new ZipEntry(DATA_VALUES_ENTRY_PREIFX + TXT_SUFFIX)) ;
	
						textFileTableObjectWriter = 
								new TextFileTableObjectWriter(new BufferedWriter(new OutputStreamWriter(outputStream))) ;
	
						textFileTableObjectWriter.setDelimiterString(TextFileHandler.TAB) ;
	
						writer = textFileTableObjectWriter ;
						break;
					case XLS:
						/*outputStream.putNextEntry(new ZipEntry(DATA_VALUES_ENTRY_PREIFX + XLS_SUFFIX)) ;
	
							JXLExcelTableObjectReader jxlExcelTableObjectReader = new JXLExcelTableObjectReader(new BufferedWriter(new OutputStreamWriter(outputStream))) ;
	
							jxlExcelTableObjectReader.setSpreadSheetName(SPREADSHEET_NAME);
	
							reader = jxlExcelTableObjectReader ;*/
						break;
					case XLSX:
						/*outputStream.putNextEntry(new ZipEntry(DATA_VALUES_ENTRY_PREIFX + XLSX_SUFFIX)) ;
	
							jxlExcelTableObjectReader = new JXLExcelTableObjectReader(new BufferedWriter(new OutputStreamWriter(outputStream))) ;
	
							jxlExcelTableObjectReader.setSpreadSheetName(SPREADSHEET_NAME);
	
							reader = jxlExcelTableObjectReader ;*/
						break;
					default:
						break;	
				}
	
				writer.writeCells(featureDataset.getValues()) ;
				
				outputStream.closeEntry();
	
				outputStream.close(); 
				
				writer.close(); 
			}
			catch (ZipException e)
			{
				throw new DatasetException(e) ;
			}
			catch (IOException e)
			{
				throw new DatasetException(e) ;
			}
		}
	}
}
