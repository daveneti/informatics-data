/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.data.feature.array;

import java.util.ArrayList;
import java.util.List;

import uno.informatics.data.dataset.FeatureDataset;
import uno.informatics.data.dataset.FeatureDatasetRow;
import uno.informatics.data.dataset.DatasetFeature;
import uno.informatics.model.impl.IdentifierImpl;

/**
 * @author Guy Davenport
 *
 */
public abstract class AbstractFeatureDataset extends IdentifierImpl implements FeatureDataset
{
	private List<DatasetFeature> features ;
	
	
	protected AbstractFeatureDataset(String name, List<DatasetFeature> features)
  {
	  super(null, name);

	  this.features = features;
  }

	protected AbstractFeatureDataset(String uniqueIdentifier, String name, List<DatasetFeature> features)
  {
	  super(uniqueIdentifier, name);
	  
	  this.features = features;
  }
	
	protected AbstractFeatureDataset(String uniqueIdentifier, String name, String desription, List<DatasetFeature> features)
  {
	  super(uniqueIdentifier, name, desription);
	  
	  this.features = features;
  }

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.Dataset#getFeatures()
	 */
	public final List<DatasetFeature> getFeatures()
	{
		return features ;
	}

	public abstract int getRowCount() ;

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.Dataset#getRowsAsArray()
	 */
	public abstract FeatureDatasetRow[] getRowsAsArray() ;

	/* (non-Javadoc)
	 * @see uno.informatics.data.tests.feature.array.array.Dataset#getRows()
	 */
	public abstract List<FeatureDatasetRow> getRows() ;

	protected final void setFeatures(List<DatasetFeature> features)
	{
		if (features != null)
			this.features = features;
		else
			this.features = new ArrayList<DatasetFeature>();
	}
}
