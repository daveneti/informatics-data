package uno.informatics.data.utils;
/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/


import static uno.informatics.common.io.TextFileHandler.COMMA;
import static uno.informatics.common.io.TextFileHandler.TAB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import uno.informatics.common.ConversionUtilities;
import uno.informatics.common.io.RowReader;
import uno.informatics.common.io.jxl.JXLExcelRowStringReader;
import uno.informatics.common.io.text.TextFileRowStringReader;
import uno.informatics.data.dataset.DatasetException;
import uno.informatics.data.feature.ColumnFeature;
import uno.informatics.data.feature.ColumnFeatureImpl;
import uno.informatics.data.io.ExcelFileProperties;
import uno.informatics.data.io.FileProperties;

public class DatasetUtils
{
	private static final String FEATURE_LABEL = DatasetUtils.class.getSimpleName() + ".featureLabel" ; 

	public static final List<ColumnFeature> generateDatasetFeatures(
			FileProperties fileProperties, boolean hasHeaders, String columnLabel, int numRows) throws DatasetException
	{
		RowReader<String> reader = null ;
		List<ColumnFeature> features = null ;

		if (fileProperties == null)
			throw new DatasetException("File properties not defined!") ;

		if (fileProperties.getFile() == null)
			throw new DatasetException("File not defined!") ;

		if (fileProperties.getFileType() == null)
			throw new DatasetException("File type not defined!") ;

		if (!fileProperties.getFile().exists())
			return features ;

		try
		{
			switch (fileProperties.getFileType())
			{
				case CSV:
					TextFileRowStringReader textFileRowStringReader = new TextFileRowStringReader(fileProperties.getFile()) ;

					textFileRowStringReader.setDelimiterString(COMMA) ;

					reader = textFileRowStringReader ;
					break;
				case TXT:
					textFileRowStringReader = new TextFileRowStringReader(fileProperties.getFile()) ;

					textFileRowStringReader.setDelimiterString(TAB) ;

					reader = textFileRowStringReader ;
					break;
				case XLS:
					JXLExcelRowStringReader jxlExcelRowStringReader = new JXLExcelRowStringReader(fileProperties.getFile()) ;

					if (fileProperties instanceof ExcelFileProperties)
						jxlExcelRowStringReader.setSpreadSheetName(((ExcelFileProperties)fileProperties).getSelectedSheet());

					reader = jxlExcelRowStringReader ;
					break;
				case XLSX:
					jxlExcelRowStringReader = new JXLExcelRowStringReader(fileProperties.getFile()) ;

					if (fileProperties instanceof ExcelFileProperties)
						jxlExcelRowStringReader.setSpreadSheetName(((ExcelFileProperties)fileProperties).getSelectedSheet());

					reader = jxlExcelRowStringReader ;
					break;
				default:
					break;	
			}

			if (reader != null && reader.ready())
			{
				features = new ArrayList<ColumnFeature>() ;

				List<String> headers ;

				List<Integer> dataTypes = new ArrayList<Integer>() ;

				int columnCount = 0 ;

				if (hasHeaders)
				{
					List<String> cells ;

					if (reader.nextRow())
					{
						cells = reader.getRowCells() ;

						dataTypes = ConversionUtilities.getDataTypes(cells) ; 			

						columnCount = cells.size() ;

						features = new ArrayList<ColumnFeature>(columnCount) ;

						dataTypes = new ArrayList<Integer>(columnCount) ;

						headers = cells ;

						while (reader.nextRow())
						{
							cells = reader.getRowCells() ;

							if (cells.size() != columnCount)
								throw new DatasetException("Rows are not all the same size!") ;

							dataTypes = ConversionUtilities.getDataTypes(cells, dataTypes) ;		
						}
					}
					else
					{
						throw new DatasetException("No columns") ;
					}
				}
				else
				{
					List<String> cells ;

					if (reader.nextRow())
					{
						cells = reader.getRowCells() ;

						dataTypes = ConversionUtilities.getDataTypes(cells) ; 			

						columnCount = cells.size() ;

						headers = new ArrayList<String>(columnCount) ;

						for (int i = 0 ; i < cells.size() ; ++i)
							headers.add(columnLabel + (i + 1)) ;

						while (reader.nextRow())
						{
							cells = reader.getRowCells() ;

							if (cells.size() != columnCount)
								throw new DatasetException("Rows are not all the same size!") ;

							dataTypes = ConversionUtilities.getDataTypes(cells, dataTypes) ;		
						}
					}    		
					else
					{
						throw new DatasetException("No columns") ;
					}
				}

				reader.close(); 

				Iterator<String> iterator = headers.iterator() ;
				Iterator<Integer> iterator2 = dataTypes.iterator() ;

				while (iterator.hasNext() && iterator2.hasNext())
				{
					features.add(new ColumnFeatureImpl(iterator.next(), iterator2.next())) ;
				}	      	
			}

			if (reader != null)
				reader.close() ;

			return features;

		}
		catch (IOException e)
		{
			throw new DatasetException(e) ;
		}
	}

	public static List<String> getSheets(FileProperties fileProperties) throws DatasetException
	{
		if (fileProperties == null)
			throw new DatasetException("File properties not defined!") ;

		if (fileProperties.getFile() == null)
			throw new DatasetException("File not defined!") ;

		if (fileProperties.getFileType() == null)
			throw new DatasetException("File type not defined!") ;

		try
		{
			switch (fileProperties.getFileType())
			{
				case CSV:
					return null ;
				case TXT:
					return null ;
				case XLS:
					JXLExcelRowStringReader jxlExcelRowStringReader = new JXLExcelRowStringReader(fileProperties.getFile()) ;

					return jxlExcelRowStringReader.getAllSpreadSheetNames() ;
				case XLSX:
					jxlExcelRowStringReader = new JXLExcelRowStringReader(fileProperties.getFile()) ;

					return jxlExcelRowStringReader.getAllSpreadSheetNames() ;
				default:
					return null ;
			}
		}
		catch (IOException e)
		{
			throw new DatasetException(e) ;
		}
	}

}
